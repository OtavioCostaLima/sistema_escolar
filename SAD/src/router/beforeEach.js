import store from '../vuex'

const isAutenticate = route => route.path.indexOf('/auth') != -1
const isLogged = () => store.getters.isLogged;

export default (to,from,next)=>{

if (!isAutenticate(to) && !isLogged()){
  next('/auth')
  }else{
    next()
  }
}
