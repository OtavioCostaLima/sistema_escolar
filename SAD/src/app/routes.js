import {routes as dashboard} from './dashboards'
import {routes as auth} from './auth'


export default [ ...auth, ...dashboard ]
