import administrador from './admisnistrador/administrador.vue'
import AdmAluno from './admisnistrador/AdmAluno.vue'
import aluno from './aluno/index.vue'
import professor from './professor/professor.vue'

export default [
  {path: '/dashboard/administrador',component: administrador},
  {path: '/dashboard/aluno', component: aluno},
  {path: '/dashboard/professor', component: professor},
  {path: '/dashboard/administrador/aluno', component: AdmAluno, name: 'dashboard.admisnistrador.aluno'}
]
